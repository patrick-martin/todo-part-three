import todosList from "./todos.json";
import {
  CLEAR_COMPLETED_TODOS,
  ADD_TODO,
  DELETE_TODO,
  TOGGLE_TODO
} from "./actions.js";

const initialState = {
  todos: todosList
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_COMPLETED_TODOS:
      //state.todos--
      //code in here will modify our state.todos
      //according to whatever the action is
      const newTodos = state.todos.filter(todo => todo.completed !== true);
      return {
        ...state,
        todos: newTodos
      };
    case ADD_TODO:
      return {
        ...state,
        todos: [...state.todos, action.payload]
      };
    case DELETE_TODO:
      return {
        ...state,
        todos: state.todos.filter(todo => todo.id !== action.payload)
      };
    case TOGGLE_TODO:
      return {
        ...state,
        todos: state.todos.map(todo =>
          todo.id === action.payload
            ? { ...todo, completed: !todo.completed }
            : todo
        )
      };
    default:
      return state;
  }
};

export default reducer;
