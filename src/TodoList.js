import React, { Component } from "react";
import TodoItem from "./TodoItem";
import { toggleTodo, deleteTodo } from "./actions.js";
import { connect } from "react-redux";

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map(todo => (
            <TodoItem
              handleDeleteTodo={() => this.props.deleteTodo(todo.id)} //maybe pass todo.id as arg
              title={todo.title}
              key={todo.id}
              completed={todo.completed}
              handleToggleTodo={() => this.props.toggleTodo(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}
//*NOT NECESSARY-- not needing state here */
// const mapStateToProps = state => {
//   return {
//     todos: state.todos
//   };
// };

const mapDispatchToProps = {
  toggleTodo,
  deleteTodo
};

export default connect(
  null,
  mapDispatchToProps
)(TodoList);
